import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
 {
  path: 'products',
    loadChildren:() => import('mfe1/Module').then(m => m.MfeOneModule)
  },

  {
    path: 'cart',
    loadChildren:() => import('mfe2/Module').then(m => m.MfeTwoModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
